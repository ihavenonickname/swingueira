# Custo

## Infra

Preços na [Digital Ocean](https://www.digitalocean.com/pricing/).

| Serviço              | Quantidade | Detalhes                             | Preço (dólar)  |
|:--------------------:|:----------:|:------------------------------------:|:--------------:|
| Servidor             | 3          | 1GB RAM, 25GB SSD, 1TB transferência | 5              |
| Balanceador de carga | 1          | Let's Encript, HTTP/2                | 10             |
| Adicionar de espaço  | 1          | 1GB SSD (para banco de dados)        | 10             |
| CDN                  | 1          | 250GB espaço, 1TB transferência      | 5              |


**R$150,00**/mês ($40,00)

## Domínio

Preços no [registro.br](https://registro.br/ajuda.html?secao=pagamentoDominio).

**R$3,00**/mês

## Total

**R$153,00**/mês